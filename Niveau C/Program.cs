﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niveau_C
{
    class Program
    {
        static void Main(string[] args)
        {

            //VARIABLES DE DONNEES

            int nbEssais = 49; //Nombre d'itérations à calculer à partir de 1 (INDIQUER A LA CONFIGURATION)
            int nbExperience = 3; //Nombre de fois qu'on recommence l'expérience (INDIQUER A LA CONFIGURATION)
            int nbRedemarrage = 3; //Nombre de fois qu'on recommence tout, utile pour les stats (INDIQUER A LA CONFIGURATION)
            int startAction = 0; //Numéro de l'action de départ (INDIQUER A LA CONFIGURATION)
            int numAction = 0; //Numéro de l'action en cours 
            String[] tabActionName = new String[3]; //Nom de chaque action (INDIQUER A LA CONFIGURATION)
            int[,] tabActionChances = new int[3, 4]; //Chances de chaque action (INDIQUER LES CHANCES AINSI QUE LE NOMBRE DE POSSIBILITES SI SUPERIEUR A 3)
            int[,] tabActionPassages = new int[3, 4]; //Numéro d'action suivante (INDIQUER LES NUMEROS POUR CHAQUE ACTION AINSI QUE POUR CHAQUE POSSIBILITE POUR CHAQUE ACTION)

            //VARIABLES INTERNES

            int nbAleatoire; //Nombre aléatoire utile au fonctionnement de l'algorithme
            Random aleatoire = new Random(); //Variable interne permettant de générer des nombres aléatoires

            //REMPLISSAGE DES TABLEAUX

            tabActionName[0] = "A";
            tabActionName[1] = "B";
            tabActionName[2] = "C";

            tabActionChances[0, 0] = 0; //Laisser à 0
            tabActionChances[0, 1] = 50;
            tabActionChances[0, 2] = 80;
            tabActionChances[0, 3] = 100;
            tabActionChances[1, 0] = 0; //Laisser à 0
            tabActionChances[1, 1] = 60;
            tabActionChances[1, 2] = 80;
            tabActionChances[1, 3] = 100;
            tabActionChances[2, 0] = 0; //Laisser à 0
            tabActionChances[2, 1] = 20;
            tabActionChances[2, 2] = 60;
            tabActionChances[2, 3] = 100;

            tabActionPassages[0, 0] = 0; //Laisser à 0
            tabActionPassages[0, 1] = 0;
            tabActionPassages[0, 2] = 1;
            tabActionPassages[0, 3] = 2;
            tabActionPassages[1, 0] = 0; //Laisser à 0
            tabActionPassages[1, 1] = 1;
            tabActionPassages[1, 2] = 0;
            tabActionPassages[1, 3] = 2;
            tabActionPassages[2, 0] = 0; //Laisser à 0
            tabActionPassages[2, 1] = 2;
            tabActionPassages[2, 2] = 0;
            tabActionPassages[2, 2] = 1;

            //tabActionName[0] = "Atelier";
            //tabActionName[1] = "Contrôle qualité";
            //tabActionName[2] = "Entrepôt";

            //tabActionChances[0, 0] = 0; //Laisser à 0
            //tabActionChances[0, 1] = 85;
            //tabActionChances[0, 2] = 100;
            //tabActionChances[0, 3] = 0;
            //tabActionChances[1, 0] = 0; //Laisser à 0
            //tabActionChances[1, 1] = 95;
            //tabActionChances[1, 2] = 100;
            //tabActionChances[1, 3] = 0;
            //tabActionChances[2, 0] = 0; //Laisser à 0
            //tabActionChances[2, 1] = 99;
            //tabActionChances[2, 2] = 100;
            //tabActionChances[2, 3] = 0;

            //tabActionPassages[0, 0] = 0; //Laisser à 0
            //tabActionPassages[0, 1] = 1;
            //tabActionPassages[0, 2] = 0;
            //tabActionPassages[1, 0] = 0; //Laisser à 0
            //tabActionPassages[1, 1] = 2;
            //tabActionPassages[1, 2] = 0;
            //tabActionPassages[2, 0] = 0; //Laisser à 0
            //tabActionPassages[2, 1] = 2;
            //tabActionPassages[2, 2] = 1;

            ///VARIABLES STATISTIQUES

            int[,] nbEvenements = new int[3, nbExperience];
            int[] sommeEvenements = new int[3];

            ///ALGORITHME
            for (int numRedemarrage = 0; numRedemarrage < nbRedemarrage; numRedemarrage++)
            {

                nbEvenements = new int[3, nbExperience];
                sommeEvenements = new int[3];

                for (int numExperience = 0; numExperience < nbExperience; numExperience++)
                {
                    numAction = startAction; //Initialisation du numéro de départ
                    //Affichage de l'événement
                    nbEvenements[numAction, numExperience]++; //Mise à jour des statistiques
                    for (int essai = 1; essai <= nbEssais; essai++)
                    {
                        nbAleatoire = aleatoire.Next(100) + 1; //Génération d'un nombre entier entre 1 et 100
                        for (int actionsPossibles = 1; actionsPossibles <= 3; actionsPossibles++)
                        {
                            if (nbAleatoire <= tabActionChances[numAction, actionsPossibles] && nbAleatoire > tabActionChances[numAction, actionsPossibles - 1])
                            {
                                numAction = tabActionPassages[numAction, actionsPossibles]; //Modification du numéro d'action pour l'action suivante
                                break;
                            }
                        }
                        //Affichage de l'événement
                        nbEvenements[numAction, numExperience]++; //Mise à jour des statistiques
                    }
                }


                ///STATISTIQUES

                for (int afficheExperience = 0; afficheExperience < nbExperience; afficheExperience++)
                {
                    Console.WriteLine("--------------------------------------------------");
                    Console.WriteLine("Evenement 1 : " + nbEvenements[0, afficheExperience]);
                    Console.WriteLine("Evenement 2 : " + nbEvenements[1, afficheExperience]);
                    Console.WriteLine("Evenement 3 : " + nbEvenements[2, afficheExperience]);
                    sommeEvenements[0] = sommeEvenements[0] + nbEvenements[0, afficheExperience];
                    sommeEvenements[1] = sommeEvenements[1] + nbEvenements[1, afficheExperience];
                    sommeEvenements[2] = sommeEvenements[2] + nbEvenements[2, afficheExperience];
                }
                Console.WriteLine("Le grand Pactouille n'a pas encore décidé quoi écrire ici (Evenement 1) : " + sommeEvenements[0]);
                Console.WriteLine("Le grand Pactouille n'a pas encore décidé quoi écrire ici (Evenement 2) : " + sommeEvenements[1]);
                Console.WriteLine("Le grand Pactouille n'a pas encore décidé quoi écrire ici (Evenement 3) : " + sommeEvenements[2]);
            }

            Console.Read();
        }
    }
}
